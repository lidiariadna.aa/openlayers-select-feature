# OpenLayers / Select Features.

En este ejemplo, se muestra cómo manejar la selección de características en un mapa de _OL_ mediante diferentes tipos de interacción, adaptandose a diferentes tipos de patrones de uso.

Una vez creada la capa vectorial, inicializar el mapa y definir los estilos de la capa, se definen las variables que permitirán hacer la selección con un solo click(Single-click), Click, al pasar el mouse (Hover) o con Alt+Click. 

Primero se define la función `changeInteraction` que ajusta la interacción de selección actual basada en la elección del usuario. Se remueve cualquier interacción previa de selección para asegurar que solo la interacción deseada este activa en cualquier momento.

```js
const changeInteraction = function () {
  if (select !== null) {
    map.removeInteraction(select);
  }
  const value = selectElement.value;
  if (value == 'singleclick') {
    select = selectSingleClick;
  } else if (value == 'click') {
    select = selectClick;
  } else if (value == 'pointermove') {
    select = selectPointerMove;
  } else if (value == 'altclick') {
    select = selectAltClick;
  } else {
    select = null;
  }
  ```
  Después se registra un evento `select` a la interacción activa. Este evento actualizará un evento _HTML_ con el id `status` para mostrar información sobre las características seleccionadas.

  ```js
  if (select !== null) {
    map.addInteraction(select);
    select.on('select', function (e) {
      document.getElementById('status').innerHTML =
        '&nbsp;' +
        e.target.getFeatures().getLength() +
        ' selected features (last operation selected ' +
        e.selected.length +
        ' and deselected ' +
        e.deselected.length +
        ' features)';
    });
  }
};
```

>Nota: cuando se utiliza un solo clic, los doble clic no seleccionarán funciones. Esto a diferencia de Click, donde un doble clic seleccionará la característica y ampliará el mapa (debido a la interacción DoubleClickZoom). Un solo clic responde menos que un clic debido al retraso que utiliza para detectar los dobles clics.

Mediante este enfoque, se ofrece al usuario flexibilidad en la forma de interactuar con el mapa, mejorando la experiencia.

